package com.ks.others;

import com.ks.lib.tcp.Cliente;
import com.ks.lib.tcp.Servidor;
import com.ks.lib.tcp.Tcp;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        try
        {
            if (args.length > 0)
            {
                Tcp conexion;
                if (args[0].equals("servidor"))
                {
                    conexion = new Servidor();
                    conexion.setPuerto(5000);
                    conexion.conectar();

                    Comunicaciones.addTCP(conexion);
                }
                else
                {
                    for (int i=0; i<5; i++)
                    {
                        conexion = new Cliente();
                        conexion.setIP("localhost");
                        conexion.setPuerto(5000);
                        conexion.setEventos(new Comunicaciones());
                        conexion.conectar();
                        Comunicaciones.addTCP(conexion);
                    }

                }
                Thread.sleep(5000);
                datos hilo = new datos();
                hilo.setDaemon(false);
                hilo.start();
                Thread.sleep(10);
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

    }
}
