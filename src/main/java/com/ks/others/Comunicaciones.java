package com.ks.others;

import com.ks.lib.tcp.Cliente;
import com.ks.lib.tcp.EventosTCP;

import com.ks.lib.tcp.Tcp;

import java.util.EventListener;
import java.util.LinkedList;
import java.util.Queue;

public class Comunicaciones implements EventosTCP
{
    private static int cont;
    private static LinkedList<Tcp> VMcolaConexiones;

    private int id;

    static
    {
        cont = 0;
        VMcolaConexiones = new LinkedList<>();
    }

    public Comunicaciones()
    {
        cont += 1;
        id = cont;
    }

    @Override
    public void conexionEstablecida(Cliente cliente)
    {
        System.out.println(id + ": Se establecio la conexion: " + cliente.toString());
    }

    @Override
    public void errorConexion(String s)
    {
        System.out.println(id + ": Error de conexion: " + s);
    }

    @Override
    public void datosRecibidos(String s, byte[] bytes, Tcp tcp)
    {
        System.out.println(id + ": Datos de entrada: " + s + " del cliente: " + tcp.toString());
    }

    @Override
    public void cerrarConexion(Cliente cliente)
    {
        System.out.println(id + ": Se cerro la conexion: " + cliente.toString());
    }

    public static void addTCP(Tcp tcp)
    {
        VMcolaConexiones.add(tcp);
    }

    public static void escribir(String mensaje)
    {
        for (Tcp conexion : VMcolaConexiones)
        {
            conexion.enviar(mensaje);
        }
    }
}
